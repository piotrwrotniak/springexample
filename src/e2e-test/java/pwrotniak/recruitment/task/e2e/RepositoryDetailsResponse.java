package pwrotniak.recruitment.task.e2e;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
class RepositoryDetailsResponse {
    @JsonProperty("cloneUrl")
    private String cloneUrl;

    @JsonProperty("createdAt")
    private String createdAt;

    @JsonProperty("description")
    private String description;

    @JsonProperty("fullName")
    private String fullName;

    @JsonProperty("stars")
    private Integer stars;
}
