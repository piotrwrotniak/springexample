package pwrotniak.recruitment.task.e2e;

import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import static java.lang.String.format;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.HttpStatus.OK;

public class E2ETest_GitHubApiClient {

    //this could be moved to some external config file
    private final static String apiHost = "localhost";
    private final static Integer apiPort = 8080;

    private final String endpoint = format("http://%s:%d", apiHost, apiPort);

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void shouldDownloadAndMapInfo() {

        final String url = format(endpoint + "/%s/%s/%s", "repositories", "spring-projects", "spring-framework");
        final ResponseEntity<RepositoryDetailsResponse> repositoryDetails = new RestTemplate().getForEntity(url, RepositoryDetailsResponse.class);
        assertNotNull(repositoryDetails);
        assertEquals(OK, repositoryDetails.getStatusCode());

        final RepositoryDetailsResponse body = repositoryDetails.getBody();
        assertEquals("https://github.com/spring-projects/spring-framework.git", body.getCloneUrl());
        assertEquals("2010-12-08T04:04:45Z", body.getCreatedAt());
        assertEquals("The Spring Framework", body.getDescription());
        assertEquals("spring-projects/spring-framework", body.getFullName());
        assertNotNull(body.getStars());
    }

    @Test
    public void shouldThrow404ForNonExistingRepository() {
        //expect
        final HttpClientExceptionMatcher notFoundExceptionMatcher = new HttpClientExceptionMatcher(NOT_FOUND);
        expectedException.expect(notFoundExceptionMatcher);
        //when
        final String url = format(endpoint + "/%s/%s/%s", "repositories", "spring-projects", "not-existing-repository");
        new RestTemplate().getForEntity(url, Object.class);
    }

    private static class HttpClientExceptionMatcher extends TypeSafeMatcher<HttpClientErrorException> {

        private final HttpStatus matchingStatus;

        HttpClientExceptionMatcher(HttpStatus matchingStatus) {
            this.matchingStatus = matchingStatus;
        }

        @Override
        protected boolean matchesSafely(HttpClientErrorException givenException) {
            return givenException.getStatusCode().equals(matchingStatus);
        }

        @Override
        public void describeTo(Description description) {
            description
                    .appendText("exception status code should equal ")
                    .appendValue(matchingStatus.value());
        }
    }

}
