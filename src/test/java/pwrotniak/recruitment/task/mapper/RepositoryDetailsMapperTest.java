package pwrotniak.recruitment.task.mapper;

import org.junit.Test;
import pwrotniak.recruitment.task.dto.GitHubRepositoryDetailsResponseDto;
import pwrotniak.recruitment.task.dto.RepositoryDetailsDto;

import java.time.Instant;

import static org.junit.Assert.assertEquals;

public class RepositoryDetailsMapperTest {

    private final String CLONE_URL = "cloneUrl";
    private final Instant CREATED_AT = Instant.now();
    private final String DESCRIPTION = "description";
    private final String FULL_NAME = "fullName";
    private final Integer STARS = 123;

    private RepositoryDetailsMapper mapper = new RepositoryDetailsMapper();

    @Test
    public void shouldMapDtosProperly() {
        //given
        GitHubRepositoryDetailsResponseDto sourceDto = createRepositoryDetailsResponseDto();
        //when
        final RepositoryDetailsDto resultDto = mapper.map(sourceDto);
        //then

        //could think about some other solution not to write asserts for each field,
        //though since it's only 5 fields I will leave it as it is
        assertEquals(resultDto.getCloneUrl(), sourceDto.getCloneUrl());
        assertEquals(resultDto.getCreatedAt(), sourceDto.getCreatedAt());
        assertEquals(resultDto.getDescription(), sourceDto.getDescription());
        assertEquals(resultDto.getFullName(), sourceDto.getFullName());
        assertEquals(resultDto.getStars(), sourceDto.getStars());
    }

    private GitHubRepositoryDetailsResponseDto createRepositoryDetailsResponseDto() {
        return GitHubRepositoryDetailsResponseDto.builder()
                .cloneUrl(CLONE_URL)
                .createdAt(CREATED_AT)
                .description(DESCRIPTION)
                .fullName(FULL_NAME)
                .stars(STARS)
                .build();
    }

}