package pwrotniak.recruitment.task.client;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockserver.integration.ClientAndServer;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import pwrotniak.recruitment.task.dto.GitHubRepositoryDetailsResponseDto;
import pwrotniak.recruitment.task.exception.GitHubResourceNotFoundException;

import java.io.IOException;
import java.text.ParseException;
import java.time.OffsetDateTime;

import static java.lang.String.format;
import static java.nio.charset.StandardCharsets.UTF_8;
import static org.junit.Assert.assertEquals;
import static org.mockserver.model.HttpRequest.request;
import static org.mockserver.model.HttpResponse.response;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.HttpStatus.OK;

public class GitHubApiClientTest {

    private final String HOST = "localhost";
    private final Integer PORT = 1080;

    private final String MOCK_API_URL = format("http://%s:%d/repos", HOST, PORT);
    private final GitHubApiClient gitHubApiClient = new GitHubApiClient(MOCK_API_URL);

    private final String OWNER = "owner";
    private final String EXISTING_REPOSITORY_NAME = "repositoryName";

    private ClientAndServer mockGitHubApiServer;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void setup() throws IOException {
        Resource resource = new ClassPathResource("okResponse.json");
        String okResponse = FileUtils.readFileToString(resource.getFile(), UTF_8);

        mockGitHubApiServer = ClientAndServer.startClientAndServer(PORT);
        mockGitHubApiServer
                .when(
                        request()
                                .withPath(format("/repos/%s/%s", OWNER, EXISTING_REPOSITORY_NAME))
                                .withHeader("Accept", "application/vnd.github.v3+json")
                                .withMethod("GET")
                )
                .respond(
                        response()
                                .withStatusCode(OK.value())
                                .withHeader("Content-Type", "application/json; charset=utf-8")
                                .withBody(okResponse)
                );

        mockGitHubApiServer
                .when(request().withMethod("GET"))
                .respond(
                        response()
                                .withStatusCode(NOT_FOUND.value())
                );
    }

    @Test
    public void shouldDownloadAndMapInfoToDto() throws ParseException {
        //when
        final GitHubRepositoryDetailsResponseDto dto = gitHubApiClient.getRepositoryDetails(OWNER, EXISTING_REPOSITORY_NAME);
        //then
        assertEquals("https://github.com/spring-projects/spring-framework.git", dto.getCloneUrl());
        assertEquals(OffsetDateTime.parse("2010-12-08T04:04:45Z").toInstant(), dto.getCreatedAt());
        assertEquals("The Spring Framework", dto.getDescription());
        assertEquals("spring-projects/spring-framework", dto.getFullName());
        assertEquals(new Integer(16317), dto.getStars());
    }

    @Test
    public void shouldThrow404IfNoSuchRepositoryFound() {
        //expect
        expectedException.expect(GitHubResourceNotFoundException.class);
        //when
        final GitHubRepositoryDetailsResponseDto dto = gitHubApiClient.getRepositoryDetails(OWNER, "notExistingRepository");
    }

    @After
    public void shutdown() {
        mockGitHubApiServer.stop();
    }
}