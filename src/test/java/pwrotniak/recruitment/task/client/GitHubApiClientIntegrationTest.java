package pwrotniak.recruitment.task.client;

import org.junit.Test;
import pwrotniak.recruitment.task.dto.GitHubRepositoryDetailsResponseDto;

import java.time.OffsetDateTime;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

//could be moved to integration-tests phase, though there is no context loading - testing integration only between client and external api
public class GitHubApiClientIntegrationTest {

    private GitHubApiClient gitHubApiClient = new GitHubApiClient("https://api.github.com/repos");

    @Test
    public void shouldDownloadAndMapInfoToDto() {
        //given
        String OWNER = "spring-projects";
        String REPOSITORY_NAME = "spring-framework";
        //when
        final GitHubRepositoryDetailsResponseDto repositoryDetails = gitHubApiClient.getRepositoryDetails(OWNER, REPOSITORY_NAME);
        //then
        assertNotNull(repositoryDetails);

        assertEquals("https://github.com/spring-projects/spring-framework.git", repositoryDetails.getCloneUrl());
        assertEquals(OffsetDateTime.parse("2010-12-08T04:04:45Z").toInstant(), repositoryDetails.getCreatedAt());
        assertEquals("The Spring Framework", repositoryDetails.getDescription());
        assertEquals("spring-projects/spring-framework", repositoryDetails.getFullName());
        assertNotNull(repositoryDetails.getStars()); //since this will change
    }

}