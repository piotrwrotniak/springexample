package pwrotniak.recruitment.task.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

import java.time.Instant;

@Data
@Builder
public class GitHubRepositoryDetailsResponseDto {

    @JsonProperty("clone_url")
    private String cloneUrl;

    //converting already ISO-formatted time to Instant,
    //to make it more useful if any data manipulation/formatting should happen
    @JsonProperty("created_at")
    private Instant createdAt;

    @JsonProperty("description")
    private String description;

    @JsonProperty("full_name")
    private String fullName;

    @JsonProperty("stargazers_count")
    private Integer stars;


}
