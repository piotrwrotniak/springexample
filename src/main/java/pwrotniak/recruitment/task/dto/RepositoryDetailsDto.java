package pwrotniak.recruitment.task.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

import java.time.Instant;

import static com.fasterxml.jackson.annotation.JsonFormat.Shape.STRING;

@Data
@Builder
//@JsonPropertyOrder(alphabetic = true) //the order as in given pdf
public class RepositoryDetailsDto {

    @JsonProperty("fullName")
    private String fullName;

    @JsonProperty("description")
    private String description;

    @JsonProperty("cloneUrl")
    private String cloneUrl;

    @JsonProperty("stars")
    private Integer stars;

    @JsonProperty("createdAt")
    @JsonFormat(shape = STRING)
    private Instant createdAt;
}
