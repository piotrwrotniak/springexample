package pwrotniak.recruitment.task.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pwrotniak.recruitment.task.client.GitHubApiClient;
import pwrotniak.recruitment.task.dto.GitHubRepositoryDetailsResponseDto;
import pwrotniak.recruitment.task.dto.RepositoryDetailsDto;
import pwrotniak.recruitment.task.mapper.RepositoryDetailsMapper;

@RequiredArgsConstructor
@Service
public class RepositoryService {
    private final GitHubApiClient gitHubApiClient;
    private final RepositoryDetailsMapper mapper;

    public RepositoryDetailsDto getRepositoryDetails(String owner, String repositoryName) {
        final GitHubRepositoryDetailsResponseDto repositoryDetails = gitHubApiClient.getRepositoryDetails(owner, repositoryName);
        return mapper.map(repositoryDetails);
    }
}
