package pwrotniak.recruitment.task.config;

import org.apache.commons.io.FileUtils;
import org.mockserver.integration.ClientAndServer;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.IOException;

import static java.lang.String.format;
import static java.nio.charset.StandardCharsets.UTF_8;
import static org.mockserver.model.HttpRequest.request;
import static org.mockserver.model.HttpResponse.response;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.HttpStatus.OK;

//I would think about extracting this to a different container,
//so that it is not a part of this app.
@Profile("mocks")
@Component
public class MockServer {

    //extracted here - could be part of the properties
    private final Integer PORT = 1080;
    private final String OWNER = "spring-projects";
    private final String EXISTING_REPOSITORY_NAME = "spring-framework";

    private ClientAndServer mockGitHubApiServer;

    //this is a duplicate of source in GitHubApiClientIntegrationTest
    //no extraction since I want the e2e tests to be independent of the component/integration tests
    @PostConstruct
    public void setup() throws IOException {
        Resource resource = new ClassPathResource("okResponse.json");
        String okResponse = FileUtils.readFileToString(resource.getFile(), UTF_8);

        mockGitHubApiServer = ClientAndServer.startClientAndServer(PORT);
        mockGitHubApiServer
                .when(
                        request()
                                .withPath(format("/repos/%s/%s", OWNER, EXISTING_REPOSITORY_NAME))
                                .withHeader("Accept", "application/vnd.github.v3+json")
                                .withMethod("GET")
                )
                .respond(
                        response()
                                .withStatusCode(OK.value())
                                .withHeader("Content-Type", "application/json; charset=utf-8")
                                .withBody(okResponse)
                );

        mockGitHubApiServer
                .when(request().withMethod("GET"))
                .respond(
                        response()
                                .withStatusCode(NOT_FOUND.value())
                );
    }


    @PreDestroy
    public void shutdown() {
        mockGitHubApiServer.stop();
    }
}
