package pwrotniak.recruitment.task.mapper;

import org.springframework.stereotype.Component;
import pwrotniak.recruitment.task.dto.GitHubRepositoryDetailsResponseDto;
import pwrotniak.recruitment.task.dto.RepositoryDetailsDto;

@Component
public class RepositoryDetailsMapper {

    public RepositoryDetailsDto map(GitHubRepositoryDetailsResponseDto dto) {
        return RepositoryDetailsDto.builder()
                .cloneUrl(dto.getCloneUrl())
                .createdAt(dto.getCreatedAt())
                .description(dto.getDescription())
                .fullName(dto.getFullName())
                .stars(dto.getStars())
                .build();
    }
}
