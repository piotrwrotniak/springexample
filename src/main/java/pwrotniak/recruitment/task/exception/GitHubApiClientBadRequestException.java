package pwrotniak.recruitment.task.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class GitHubApiClientBadRequestException extends RuntimeException {
    public GitHubApiClientBadRequestException(Exception exception) {
        super(exception);
    }
}
