package pwrotniak.recruitment.task.exception;

import org.springframework.web.bind.annotation.ResponseStatus;

import static org.springframework.http.HttpStatus.NOT_FOUND;

@ResponseStatus(value = NOT_FOUND, reason="There is no such GitHub resource")
public class GitHubResourceNotFoundException extends RuntimeException {
}
