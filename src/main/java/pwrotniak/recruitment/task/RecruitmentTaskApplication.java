package pwrotniak.recruitment.task;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableAutoConfiguration
public class RecruitmentTaskApplication {
    public static void main(String[] args) {
        SpringApplication.run(RecruitmentTaskApplication.class, args);
    }
}
