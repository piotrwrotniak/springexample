package pwrotniak.recruitment.task.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pwrotniak.recruitment.task.dto.RepositoryDetailsDto;
import pwrotniak.recruitment.task.service.RepositoryService;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

@RestController
@RequestMapping("/repositories")
@RequiredArgsConstructor
public class RepositoriesController {

    private final RepositoryService repositoryService;

    @GetMapping(value = "/{owner}/{repository-name}",
                produces = APPLICATION_JSON_UTF8_VALUE)
    public RepositoryDetailsDto getRepositoryDetails(@PathVariable("owner") String owner,
                                                     @PathVariable("repository-name") String repositoryName) {
        return repositoryService.getRepositoryDetails(owner, repositoryName);
    }
}
