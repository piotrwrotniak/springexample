package pwrotniak.recruitment.task.client;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResponseErrorHandler;
import org.springframework.web.client.RestTemplate;
import pwrotniak.recruitment.task.dto.GitHubRepositoryDetailsResponseDto;
import pwrotniak.recruitment.task.exception.GitHubApiClientBadRequestException;
import pwrotniak.recruitment.task.exception.GitHubResourceNotFoundException;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import static java.lang.String.format;
import static org.springframework.http.HttpMethod.GET;

@Component
public class GitHubApiClient {

    private final String gitHubApiUrl;

    public GitHubApiClient(@Value("${github.api.url}") String gitHubApiUrl) {
        this.gitHubApiUrl = gitHubApiUrl;
    }

    public GitHubRepositoryDetailsResponseDto getRepositoryDetails(String owner, String repositoryName) {
        final RestTemplate restTemplate = getGitHubRestTemplate();
        final URI uri = getGitHubRepositoryApiURI(owner, repositoryName);
        final RequestEntity<String> requestEntity = new RequestEntity<>(getGitHubApiHeaders(), GET, uri);
        final ResponseEntity<GitHubRepositoryDetailsResponseDto> response = restTemplate.exchange(requestEntity, GitHubRepositoryDetailsResponseDto.class);
        return response.getBody();
    }

    private RestTemplate getGitHubRestTemplate() {
        final RestTemplate restTemplate = new RestTemplate();
        restTemplate.setErrorHandler(new GitHubClientErrorHandler());
        return restTemplate;
    }

    private MultiValueMap<String, String> getGitHubApiHeaders() {
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("Accept", "application/vnd.github.v3+json");
        return headers;
    }

    private URI getGitHubRepositoryApiURI(String owner, String repositoryName) {
        String url = format("%s/%s/%s", gitHubApiUrl, owner, repositoryName);
        try {
            return new URI(url);
        } catch (URISyntaxException exception) {
            throw new GitHubApiClientBadRequestException(exception);
        }
    }

    private static class GitHubClientErrorHandler implements ResponseErrorHandler {

        @Override
        public boolean hasError(ClientHttpResponse response) throws IOException {
            final HttpStatus statusCode = response.getStatusCode();
            return statusCode.series() == HttpStatus.Series.CLIENT_ERROR ||
                    statusCode.series() == HttpStatus.Series.SERVER_ERROR;
        }

        @Override
        public void handleError(ClientHttpResponse response) throws IOException {
            //probably there's going to be more error handling as the client grows
            //so 'switch' used
            switch (response.getStatusCode()) {
                case NOT_FOUND:
                    throw new GitHubResourceNotFoundException();
                default:
                    throw new HttpClientErrorException(response.getStatusCode(), response.getStatusText());
            }
        }
    }
}
